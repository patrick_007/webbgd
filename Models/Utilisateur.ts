export interface Utilisateur {
    uid?: string;
    email: string;
    displayName: string;
    photoURL: string;
    emailVerified: boolean;
    phoneNumber: any;
 }